if vl:os_shortid_get() == "win11" then
    if vl:dom_xpath("//domain/devices/tpm") == nil then
        vl:add_warning(vl.WarningDomain_Domain, vl.WarningLevel_Error,
        "TPM is missing in domain XML. Windows 11 requires TPM to boot.")
    end
end
