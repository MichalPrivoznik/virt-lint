import sys
import libvirt

def check():
    if vl.os_shortid_get() == "win11":
        if not vl.dom_xpath("//domain/devices/tpm"):
            vl.add_warning(vl.WarningDomain_Domain, vl.WarningLevel_Error,
                           "TPM is missing in domain XML. Windows 11 requires TPM to boot.")

check()
